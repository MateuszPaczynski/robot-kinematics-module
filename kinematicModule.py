import math
import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from pynput.keyboard import Key, Listener

class Point:
	x = 0.0
	y = 0.0

	def __str__(self):
		return "x: {}, y: {}".format(self.x, self.y)


class Joint:
    # should I create base joint class for both cases?
    # That will be probalby a good idea especially that the only difference will be a motion type
    # let start with the simple rotational joint example for 2d space and then I will expand

    def __init__(self, parentJoint, length, initAngle):
        self.root = parentJoint
        self.length = length
        self._angle = initAngle
        self.originPos = Point()
        self.endEffectorPos = Point()
        if self.root != None:
            self.calcOriginPosition()
        self.calcEndEffectorPosition()

    def calcOriginPosition(self):
        self.originPos.x = self.root.endEffectorPos.x
        self.originPos.y = self.root.endEffectorPos.y

    def calcEndEffectorPosition(self):
        self.endEffectorPos.x = self.originPos.x + self.length * math.sin(self._angle)
        self.endEffectorPos.y = self.originPos.y + self.length * math.cos(self._angle)

    def getAngle(self):
        return self._angle


class RotationalJoint(Joint):
    def __init__(self, parentJoint, length, initAngle, angleLimit):
        super(RotationalJoint, self).__init__(parentJoint, length, initAngle)
        self.limitLeft = self._angle + angleLimit[0]
        self.limitRight = self._angle + angleLimit[1]

    def setAngle(self, angle):
        if self.root != None:
            self._angle = angle + self.root.getAngle()
        else:
            self._angle = angle
        self.checkAngleLimit()

    def checkAngleLimit(self):
        if self._angle < self.limitLeft:
            self._angle = self.limitLeft
        elif self._angle > self.limitRight:
            self._angle = self.limitRight

class TransmisionJoint(Joint):
    def __init__(self, parentJoint, length, initAngle, angleLimit):
        super(TransmisionJoint, self).__init__(self, parentJoint, length, initAngle, angleLimit)


def on_press(key):
    print(key)
    if key.char == 'q':
        quit()

mousePosX = 0
mousePosY = 0


def onMotion(event):
    global mousePosX
    global mousePosY
    mousePosX = event.xdata
    mousePosY = event.ydata
    print('Mouse plot position x=%d, y=%d' %
          (event.xdata, event.ydata))


fig= plt.figure()
ax = fig.gca()
ax.set_xlim([-200, 200])
ax.set_ylim([0, 400])
xdata, ydata= [0], [0]
ln, = plt.plot([], [], 'b')

root = RotationalJoint(None, 200, 0, [-math.pi/4, math.pi/4])
print('root')
jnt1 = RotationalJoint(root, 200, 0, [-math.pi/2, math.pi/2])
print('jnt1')

cid = fig.canvas.mpl_connect('motion_notify_event', onMotion)



def plot_update(frame):
    #somehow I need to send data about joins positions
    try:
        angleDemand = math.atan2(mousePosY, mousePosX)
    except:
        angleDemand = 0

    root.setAngle(-angleDemand + math.pi / 2)
    jnt1.setAngle(-angleDemand + math.pi / 2)
    print("Input angle {}, with limits: {}".format(angleDemand, root.getAngle()))

    root.calcEndEffectorPosition()
    jnt1.calcOriginPosition()
    jnt1.calcEndEffectorPosition()
    xdata = [root.originPos.x, root.endEffectorPos.x, jnt1.originPos.x, jnt1.endEffectorPos.x]
    ydata = [root.originPos.y, root.endEffectorPos.y, jnt1.originPos.y, jnt1.endEffectorPos.y]
    ln.set_data(xdata, ydata)
    return ln,


if __name__ == '__main__':
    # implement a rotational joint and the rotational join
    # make it possible to connect joints in any configuration
    # create class robot which will contain joints
    # add limit capability to the joins
    # joins should be able to follow mouse -> mouse/keyboard input
    print("2D kinematic test")

    # need to set up a 2d animation scene, ech that is annoying
    # initial plan is to show only static origin root, later on, I will try to add some random montion to it
    # lets add simple forward kinematic control, I will be able to control joint by joint by pressing arrows
    # with Listener(on_press=on_press) as listener:
    #     listener.join()

    ani = FuncAnimation(fig, plot_update, frames=np.linspace(0, 2 * np.pi, 128), interval=2, blit=True)
    plt.show()